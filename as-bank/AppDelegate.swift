//
//  AppDelegate.swift
//  as-bank
//
//  Created by Дмитрий Бутилов on 13.06.18.
//  Copyright © 2018 Дмитрий Бутилов. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
        // Override point for customization after application launch.
        
        UserDefaults.standard.set(false, forKey: UserKeys.userLogged)
        return true
    }

    func applicationWillResignActive(_ application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
    }

    func applicationDidEnterBackground(_ application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
        
        // по идее нужно, но нужнно обрабатывать
        //deleteUserSettings()
    }

    func applicationWillEnterForeground(_ application: UIApplication) {
        // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
    }

    func applicationDidBecomeActive(_ application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    }

    func applicationWillTerminate(_ application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
        deleteUserSettings()
    }
    
    func deleteUserSettings() {
        let isLogged = UserDefaults.standard.bool(forKey: UserKeys.userLogged)
        if (isLogged == true) {
        let token :String? = UserDefaults.standard.string(forKey: UserKeys.userToken)
        let login :String? = UserDefaults.standard.string(forKey: UserKeys.userLogin)
        let header: HTTPHeaders = ["Content-Type": "application/json", "charset": "utf-8", "login": login!, "token": token!]
            let url: String = ServerSettings.address + "/tokens/" + token!
            request(url, method: .delete, encoding: JSONEncoding.default, headers: header).responseJSON { responseJSON in
                    guard let statusCode = responseJSON.response?.statusCode else { return }
                    print("statusCode: ", statusCode)
                    if (statusCode == 200) {
                        print("deleted")
                    }
                }
        
        UserDefaults.standard.removeObject(forKey: UserKeys.userLogin)
        UserDefaults.standard.removeObject(forKey: UserKeys.userToken)
        UserDefaults.standard.set(false, forKey: UserKeys.userLogged)
        }
    }
}
