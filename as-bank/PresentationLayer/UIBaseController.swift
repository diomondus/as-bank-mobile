//
//  UIBaseController.swift
//  as-bank
//
//  Created by Дмитрий Бутилов on 17.06.18.
//  Copyright © 2018 Дмитрий Бутилов. All rights reserved.
//

import Foundation
import UIKit

class UIBaseController: UIViewController, UITextFieldDelegate {
    
    func displayMyAlertMessage(userMessage :String) {
        let myAlert = UIAlertController(title:"Внимание", message: userMessage, preferredStyle: UIAlertControllerStyle.alert);
        let okAction = UIAlertAction(title:"ОК", style:UIAlertActionStyle.default, handler:nil);
        myAlert.addAction(okAction);
        self.present(myAlert, animated:true, completion:nil);
    }
    
    // Вызывается, когда пользователь кликает на view (за пределами textField)
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        if let _ = touches.first {
            view.endEditing(true)
        }
        super.touchesBegan(touches, with:event)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
}
