//
//  BillController.swift
//  as-bank
//
//  Created by Дмитрий Бутилов on 14.06.18.
//  Copyright © 2018 Дмитрий Бутилов. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON

class BillController: UIBaseController {
    @IBOutlet weak var BillTextField: UILabel!
    
    @IBAction func exitAction(_ sender: Any) {
        deleteUserSettings()
    }
    
    func deleteUserSettings() {
        let isLogged = UserDefaults.standard.bool(forKey: UserKeys.userLogged)
        if (isLogged == true) {
            let token :String? = UserDefaults.standard.string(forKey: UserKeys.userToken)
            let login :String? = UserDefaults.standard.string(forKey: UserKeys.userLogin)
            let header: HTTPHeaders = ["Content-Type": "application/json", "charset": "utf-8", "login": login!, "token": token!]
            request(ServerSettings.address + "/tokens/" + token!, method: .delete, encoding: JSONEncoding.default, headers: header).responseJSON { responseJSON in
                guard let statusCode = responseJSON.response?.statusCode else { return }
                print("statusCode: ", statusCode)
                if (statusCode == 200) {
                    print("deleted")
                }
            }
            
            UserDefaults.standard.removeObject(forKey: UserKeys.userLogin)
            UserDefaults.standard.removeObject(forKey: UserKeys.userToken)
            UserDefaults.standard.set(false, forKey: UserKeys.userLogged)
            self.dismiss(animated: true, completion: nil)
        }
    }
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    fileprivate func requestBill(_ billName: String, _ header: HTTPHeaders) {
        let url: String = ServerSettings.address + "/bills/" + billName + "/values"
        request(url, method: .get, encoding: JSONEncoding.default, headers: header).responseJSON { responseJSON in
            guard let statusCode = responseJSON.response?.statusCode else { return }
            print("statusCode: ", statusCode)
            if (statusCode == 200) {
                let swiftyJsonVar = JSON(responseJSON.result.value!).number
                self.BillTextField.text = swiftyJsonVar?.stringValue
            } else {
                self.displayMyAlertMessage(userMessage: "Ошибка");
            }
        }
    }
    
    fileprivate func requestUser(_ login: String?, _ header: HTTPHeaders) {
        let url: String = ServerSettings.address + "/users/" + login!
        request(url, method: .get, encoding: JSONEncoding.default, headers: header).responseJSON { responseJSON in
            guard let statusCode = responseJSON.response?.statusCode else { return }
            print("statusCode: ", statusCode)
            if (statusCode == 200) {
                let swiftyJsonVar = JSON(responseJSON.result.value!)
                let bill : [String: JSON] = swiftyJsonVar["bill"].dictionary!
                let billName :String = (bill["name"]?.string)!
                self.requestBill(billName, header)
            } else {
                self.displayMyAlertMessage(userMessage: "Ошибка");
            }
        }
    }
    
    override func viewDidAppear(_ animated: Bool) {
        let isLogged = UserDefaults.standard.bool(forKey: UserKeys.userLogged)
        if (isLogged == false) {
            self.dismiss(animated: true, completion: nil)
        } else {
            let token :String? = UserDefaults.standard.string(forKey: UserKeys.userToken)
            let login :String? = UserDefaults.standard.string(forKey: UserKeys.userLogin)
            let header: HTTPHeaders = ["Content-Type": "application/json", "charset": "utf-8", "token": token!]
            requestUser(login, header)
        }
    }
}
