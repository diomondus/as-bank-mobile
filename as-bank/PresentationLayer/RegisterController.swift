//
//  RegisterController.swift
//  as-bank
//
//  Created by Дмитрий Бутилов on 14.06.18.
//  Copyright © 2018 Дмитрий Бутилов. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON

class RegisterController: UIBaseController  {
    
    @IBOutlet weak var LoginTextField: UITextField!
    
    @IBOutlet weak var PasswordTextField: UITextField!
    
    @IBOutlet weak var RepeatPasswordTextField: UITextField!
    
    @IBAction func registerButtonTapped(_ sender: Any) {
        let userLogin = LoginTextField.text;
        let userPassword = PasswordTextField.text;
        let userRepeatPassword = RepeatPasswordTextField.text;
        
        // Check for empty fields
        if((userLogin?.isEmpty)! || (userPassword?.isEmpty)! || (userRepeatPassword?.isEmpty)!) {
            displayMyAlertMessage(userMessage: "Необходимо заполнить все поля");
            return;
        }
        if(userPassword != userRepeatPassword) {
            displayMyAlertMessage(userMessage: "Пароли не совпадают");
            return;
        }
        
        registrate()
    }
    
    func registrate() {
        // prepare json data
        let json: [String: Any] = ["login": LoginTextField.text!, "password" : PasswordTextField.text!]
        let header: HTTPHeaders = ["Content-Type": "application/json", "charset": "utf-8"]
        let url: String = ServerSettings.address + "/users"
        request(url, method: .post, parameters: json, encoding: JSONEncoding.default, headers: header).responseJSON { responseJSON in
            guard let statusCode = responseJSON.response?.statusCode else { return }
            print("statusCode: ", statusCode)
            if (statusCode == 200) {
                self.displaySuccessfulRegistrationAlert()
            }
            if (statusCode == 302) {
                self.displayMyAlertMessage(userMessage: "Пользователь с таким именем уже существует");
            }
        }
    }
    
    func displaySuccessfulRegistrationAlert() {
        let myAlert = UIAlertController(title:"Уведомление", message:"Регистрация прошла успешно. Спасибо!", preferredStyle: UIAlertControllerStyle.alert);
        let okAction = UIAlertAction(title:"ОК", style:UIAlertActionStyle.default){ action in
            self.dismiss(animated: true, completion:nil);
        }
        myAlert.addAction(okAction);
        self.present(myAlert, animated:true, completion:nil);
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    @IBAction func iHaveAnAccountButtonTapped(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
}
