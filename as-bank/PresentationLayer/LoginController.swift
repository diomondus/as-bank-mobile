//
//  LoginController.swift
//  as-bank
//
//  Created by Дмитрий Бутилов on 13.06.18.
//  Copyright © 2018 Дмитрий Бутилов. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON

class LoginController: UIBaseController {
    
    @IBAction func loginAction(_ sender: Any) {
        login()
    }
    
    func login() {
        let login = LoginTextField.text!;
        let json: [String: Any] = ["login": login, "password" : PasswordTestField.text!]
        let header: HTTPHeaders = ["Content-Type": "application/json", "charset": "utf-8"]
        let url: String = ServerSettings.address + "/tokens"
        request(url, method: .post, parameters: json, encoding: JSONEncoding.default, headers: header).responseJSON { responseJSON in
            guard let statusCode = responseJSON.response?.statusCode else { return }
            print("statusCode: ", statusCode)
            
            if (statusCode == 200) {
                let swiftyJsonVar = JSON(responseJSON.result.value!)
                let token = swiftyJsonVar["value"].string
                let defaults = UserDefaults.standard
                defaults.set(token, forKey: UserKeys.userToken)
                defaults.set(true, forKey: UserKeys.userLogged)
                defaults.set(login, forKey: UserKeys.userLogin)
                self.dismiss(animated: true, completion: nil)
            } else {
                self.displayMyAlertMessage(userMessage: "Неверный пароль");
            }
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    @IBOutlet weak var LoginTextField: UITextField!
    @IBOutlet weak var PasswordTestField: UITextField!
}
